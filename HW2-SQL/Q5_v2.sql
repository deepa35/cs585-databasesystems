/* Oracle Live SQL */

CREATE TABLE Chef_Dish (
Chef CHAR(1) NOT NULL,
Dish VARCHAR2(50) NOT NULL);

INSERT INTO Chef_Dish VALUES
('A','Mint chocolate brownie');
INSERT INTO Chef_Dish VALUES
('B','Upside down pineapple cake');
INSERT INTO Chef_Dish VALUES
('B','Creme brulee');
INSERT INTO Chef_Dish VALUES
('B','Mint chocolate brownie');
INSERT INTO Chef_Dish VALUES
('C','Upside down pineapple cake');
INSERT INTO Chef_Dish VALUES
('C','Creme brulee');
INSERT INTO Chef_Dish VALUES
('D','Apple pie');
INSERT INTO Chef_Dish VALUES
('D','Upside down pineapple cake');
INSERT INTO Chef_Dish VALUES
('D','Creme brulee');
INSERT INTO Chef_Dish VALUES
('E','Apple pie');
INSERT INTO Chef_Dish VALUES
('E','Upside down pineapple cake');
INSERT INTO Chef_Dish VALUES
('E','Creme brulee');
INSERT INTO Chef_Dish VALUES
('E','Bananas Foster');

CREATE TABLE Dishes(
Dish VARCHAR2(50) NOT NULL);

INSERT INTO Dishes VALUES
('Apple pie');
INSERT INTO Dishes VALUES
('Upside down pineapple cake');
INSERT INTO Dishes VALUES
('Creme brulee');

SELECT Chef FROM (SELECT Chef,LISTAGG(Dish,',') AS Full_Dish FROM 
(SELECT Chef_Dish.Chef,Dishes.Dish FROM Chef_Dish INNER JOIN Dishes ON Chef_Dish.Dish=Dishes.Dish ORDER BY Dishes.Dish) GROUP BY Chef) Ordered1
INNER JOIN 
(SELECT LISTAGG(Dish,',') AS Full_Dish FROM (SELECT * FROM Dishes ORDER BY Dish DESC) GROUP BY NULL) Ordered2 
ON
Ordered1.Full_Dish=Ordered2.Full_Dish;

/*
Summary: This query first computes the inner join between the chef-dish table and the required dishes table. The 
resulting table is then formatted using LISTAGG function which takes one chef and joins all the dishes which he knows 
to make separated by commas. The resulting table has each chef and the alphabetically sorted list of dishes he makes
 separated by comma. The list of dish table is also sorted alphabetically and combined in the same fashion as the 
 previous one. Now we just look for the chef whose list of dishes match the required dish list and output them.
*/



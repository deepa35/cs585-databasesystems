/* Oracle Live SQL */

The design of the table must be changed in this case. Instead of making room number and arrival date the
 primary key, a reservation number should be assigned and made the primary key.

The columns in the main table are RESERVE_NUM, CUST_FNAME, CUST_LNAME, ARR_DATE, DEP_DATE, 
ROOM_NUM, ACTIVITY. The columns imply Reservation/Booking number, first name of the guest, last name of 
the guest, arrival date, departure date, room number and status whether the room reservation is active
 or not(1 means active,0 means checked out). We can use a Sequence to keep track of reservation numbers.

There can be a separate room table where each room info is given. The columns in that table can be
 ROOM_NUM, ROOM_STAT, ROOM_AVAIL_DATE which implies room number, room status 
 (0 means not available, 1 means available) and room availability date. If the room status is 0, then the date at
 which the room will be vacated is given in the room availability date column. If the room status is 1, then the 
 value in this column need not be checked.

This design can solve both the problems stated in the question. This can be achieved using CHECK and triggers.

1. Problem of arrival_date>departure date: To solve this scenario we can add a check that arrival_date will be
 always less than or equal to the departure date. This can prevent the database manager from accidentally
 updating the database with wrong value. If anyone tries to put in a departure date less than arrival date, the
 system will throw an error.

CREATE TABLE HotelStays 
(RESERVE_NUM INTEGER NOT NULL, 
CUST_FNAME CHAR(30) NOT NULL,
CUST_LNAME CHAR(30) NOT NULL,
ARR_DATE DATE NOT NULL, 
DEP_DATE DATE NOT NULL, 
ROOM_NUM INTEGER NOT NULL,
ACTIVITY INTEGER NOT NULL,
PRIMARY KEY (RESERVE_NUM), 
CHECK(DEP_DATE>=ARR_DATE))

2. Problem of room allocation: In this case, the power of allocation of room should be taken away from the 
administrator. All the other data excluding the room number can be inserted by the admin and once the row is 
created, it is possible to issue a trigger. This trigger checks the Room details table and see the first room which 
has a ROOM_STAT=1. This room can be marked unavailable ie ROOM_STAT=0 and the next availability date can
 be set. The retrieved room number can be updated in the reservations table and given to the guest. Once all the
 rooms are assigned and if a booking request is made. It is possible to query the room details table to find the room
 which has the least availability date and that room can be issued to the guest, if the arrival date > availability date. 
 If the condition is not met then the request is not served. The inserted row for that reservation can be removed.

When a guest checks out, the reservation row can be marked inactive ie 0. When this happens we can assign a
 trigger to mark the room status in rooms table as 0, ie the room is available to occupy.



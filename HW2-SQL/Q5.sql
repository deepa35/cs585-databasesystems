/* Oracle Live SQL */

CREATE TABLE Chef_Dish (
Chef CHAR(1) NOT NULL,
Dish VARCHAR2(50) NOT NULL);

INSERT INTO Chef_Dish VALUES
('A','Mint chocolate brownie');
INSERT INTO Chef_Dish VALUES
('B','Upside down pineapple cake');
INSERT INTO Chef_Dish VALUES
('B','Creme brulee');
INSERT INTO Chef_Dish VALUES
('B','Mint chocolate brownie');
INSERT INTO Chef_Dish VALUES
('C','Upside down pineapple cake');
INSERT INTO Chef_Dish VALUES
('C','Creme brulee');
INSERT INTO Chef_Dish VALUES
('D','Apple pie');
INSERT INTO Chef_Dish VALUES
('D','Upside down pineapple cake');
INSERT INTO Chef_Dish VALUES
('D','Creme brulee');
INSERT INTO Chef_Dish VALUES
('E','Apple pie');
INSERT INTO Chef_Dish VALUES
('E','Upside down pineapple cake');
INSERT INTO Chef_Dish VALUES
('E','Creme brulee');
INSERT INTO Chef_Dish VALUES
('E','Bananas Foster');

CREATE TABLE Dishes(
Dish VARCHAR2(50) NOT NULL);

INSERT INTO Dishes VALUES
('Apple pie');
INSERT INTO Dishes VALUES
('Upside down pineapple cake');
INSERT INTO Dishes VALUES
('Creme brulee');

SELECT Chef FROM 
(SELECT Chef, COUNT(Dish) AS Total  FROM 
(SELECT Chef_Dish.Chef,Dishes.Dish FROM Chef_Dish INNER JOIN Dishes ON Chef_Dish.Dish=Dishes.Dish)
GROUP BY Chef)
WHERE Total=(SELECT COUNT(*) FROM Dishes) ORDER BY Chef;

/* 
What the query does:
SELECT Chef_Dish.Chef,Dishes.Dish FROM Chef_Dish INNER JOIN Dishes ON Chef_Dish.Dish=Dishes.Dish
This query performs inner join between the two tables(Table of dishes to be cooked and table containing dishes
 which the chef can cook) on the name of dishes. The result will give only those chefs who can cook either all or any
 of the required dishes. Unwanted dish data will thus be eliminated.
 
 This table is given as input to the outer query 'SELECT Chef, COUNT(Dish) AS Total  FROM (a) GROUP BY Chef'
 where (a) corresponds to the result from previous query. Here we count the number of dishes which each chef can
 prepare and name the column as 'Total'.
 
 This table is given as input to 'SELECT Chef FROM (b) WHERE Total=(SELECT COUNT(*) FROM Dishes) 
 ORDER BY Chef' where (b) corresponds to the result of previous query. Here we see which all chefs prepare same
 number of dishes as the total dishes in the required list. Only those chefs are outputted ordered alphabetically.
 
 Summary: So the query first finds all chefs who prepare dishes mentioned in the required list of dishes. Then the count
 of dishes prepared by each chef is calfulated. If the count equals total number of required dishes, then output those chefs
 */
 



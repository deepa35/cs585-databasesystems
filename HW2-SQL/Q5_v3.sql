/* Oracle Live SQL */

CREATE TABLE Chef_Dish (
Chef CHAR(1) NOT NULL,
Dish VARCHAR2(50) NOT NULL);

INSERT INTO Chef_Dish VALUES
('A','Mint chocolate brownie');
INSERT INTO Chef_Dish VALUES
('B','Upside down pineapple cake');
INSERT INTO Chef_Dish VALUES
('B','Creme brulee');
INSERT INTO Chef_Dish VALUES
('B','Mint chocolate brownie');
INSERT INTO Chef_Dish VALUES
('C','Upside down pineapple cake');
INSERT INTO Chef_Dish VALUES
('C','Creme brulee');
INSERT INTO Chef_Dish VALUES
('D','Apple pie');
INSERT INTO Chef_Dish VALUES
('D','Upside down pineapple cake');
INSERT INTO Chef_Dish VALUES
('D','Creme brulee');
INSERT INTO Chef_Dish VALUES
('E','Apple pie');
INSERT INTO Chef_Dish VALUES
('E','Upside down pineapple cake');
INSERT INTO Chef_Dish VALUES
('E','Creme brulee');
INSERT INTO Chef_Dish VALUES
('E','Bananas Foster');

CREATE TABLE Dishes(
Dish VARCHAR2(50) NOT NULL);

INSERT INTO Dishes VALUES
('Apple pie');
INSERT INTO Dishes VALUES
('Upside down pineapple cake');
INSERT INTO Dishes VALUES
('Creme brulee');

SELECT Chef FROM (SELECT COUNT(*), Chef FROM (SELECT * FROM Chef_Dish WHERE Dish IN (SELECT * FROM Dishes))
GROUP BY Chef
HAVING COUNT(*)=(SELECT COUNT(*) FROM Dishes)) 
ORDER BY Chef;

/* 
Summary: This method first selects chefs who can cook dishes mentioned in the Dishes table. Then it
computes the count of dishes which the chef can make from the required list of dishes.
Select the chef from this table who can make same number of dishes as given in the required dish list.
*/